#include "oglwidget.h"
#include <QGLWidget>
#include <QDebug>

#include "libs/SOIL.h"

OGLWidget::OGLWidget(QWidget *parent)
    : QOpenGLWidget(parent)
{
    xRot = 0;
    yRot = 0;
    zRot = 0;
}

OGLWidget::~OGLWidget()
{

}

void OGLWidget::initializeGL()
{
    glClearColor(0,0,0,1);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glEnable(GL_CULL_FACE);
    glShadeModel(GL_SMOOTH);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
    glEnable(GL_MULTISAMPLE);

    static GLfloat lightPos[4] = {0.5, 5.0, 7.0, 1.0};

    glLightfv(GL_LIGHT0, GL_POSITION, lightPos);

    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);

}

void OGLWidget::resizeGL(int width, int height)
{
    glViewport(0,0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45, (float)width/height, 0.01, 100.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(0,0,5,
              0,0,0,
              0,1,0);
}

void OGLWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glPushMatrix();
    glRotatef(xRot / 16.0f, 1.0, 0.0, 0.0);
    glRotatef(yRot / 16.0f, 0.0, 1.0, 0.0);
    glRotatef(zRot / 16.0f, 0.0, 0.0, 1.0);


    glBegin(GL_QUADS);
        //Top, green
        glColor3f(0.0f, 1.0f, 0.0f);
        glVertex3f( 1.0f, 1.5f, -1.0f); //B
        glVertex3f(-1.0f, 1.5f, -1.0f); //A
        glVertex3f(-1.0f, 1.5f,  1.0f); //D
        glVertex3f (1.0f, 1.5f,  1.0f); //C

        //Bottom, orange
        glColor3f(1.0f, 0.5f, 0.0f);
        glVertex3f( 1.0f, -1.5f,  1.0f); //G
        glVertex3f(-1.0f, -1.5f,  1.0f); //H
        glVertex3f(-1.0f, -1.5f, -1.0f); //E
        glVertex3f( 1.0f, -1.5f, -1.0f); //F

        //Left, blue
        glColor3f(0.0f, 0.0f, 1.0f);
        glVertex3f(-1.0f,  1.5f,  1.0f); //D
        glVertex3f(-1.0f,  1.5f, -1.0f); //A
        glVertex3f(-1.0f, -1.5f, -1.0f); //E
        glVertex3f(-1.0f, -1.5f,  1.0f); //H

        //Right, magenta
        glColor3f(1.0f, 0.0f, 1.0f);
        glVertex3f(1.0f,  1.5f, -1.0f); //B
        glVertex3f(1.0f,  1.5f,  1.0f); //C
        glVertex3f(1.0f, -1.5f,  1.0f); //G
        glVertex3f(1.0f, -1.5f, -1.0f); //F

        //Front, red
        glColor3f(1.0f, 0.0f, 0.0f);
        glVertex3f( 1.0f,  1.5f, 1.0f); //C
        glVertex3f(-1.0f,  1.5f, 1.0f); //D
        glVertex3f(-1.0f, -1.5f, 1.0f); //H
        glVertex3f( 1.0f, -1.5f, 1.0f); //G

        //Back, yellow
        glColor3f(1.0f, 1.0f, 0.0f);
        glVertex3f( 1.0f, -1.5f, -1.0f); //F
        glVertex3f(-1.0f, -1.5f, -1.0f); //E
        glVertex3f(-1.0f,  1.5f, -1.0f); //A
        glVertex3f( 1.0f,  1.5f, -1.0f); //B


     glEnd();

     glPopMatrix();

}

void OGLWidget::mousePressEvent(QMouseEvent *event)
{
    lastPos = event->pos();
}

void OGLWidget::mouseMoveEvent(QMouseEvent *event)
{
    int dx = event->x() - lastPos.x();
    int dy = event->y() - lastPos.y();

    if (event->buttons() & Qt::LeftButton)
    {
        setXRotation(xRot + 8 * dy);
        setYRotation(yRot + 8 * dx);
    }
    else if (event->buttons() & Qt::RightButton)
    {
        setXRotation(xRot + 8 * dy);
        setZRotation(zRot + 8 * dx);
    }
    lastPos = event->pos();
    qDebug() << "Mouse position: [" << lastPos.x() << ";" << lastPos.y() << "]";
}

void OGLWidget::normalizeAngle(int angle)
{
    while (angle < 0)
    {
        angle += 360 * 16;
    }
    while (angle > 360 * 16)
    {
        angle -= 360 * 16;
    }
}

void OGLWidget::setXRotation(int angle)
{
    normalizeAngle(angle);
    if(angle != xRot)
    {
        xRot = angle;
        update();
    }
}

void OGLWidget::setYRotation(int angle)
{
    normalizeAngle(angle);
    if(angle != yRot)
    {
        yRot = angle;
        update();
    }
}

void OGLWidget::setZRotation(int angle)
{
    normalizeAngle(angle);
    if(angle != zRot)
    {
        zRot = angle;
        update();
    }
}
